------------------
--   COMMAND    --  Use /flylikea [arg]    for example: /flylikea A_C_Crow_01
------------------
RegisterCommand('flylikea', function(source,args,rawCommand)
    Citizen.CreateThread(function()
        if GetHashKey(args[1]) == -794219200 then
            birdmodel = 0x5DF8F2C
            if not HasModelLoaded(birdmodel) then
                RequestModel(birdmodel, true)
                while not HasModelLoaded(npcModel) do
                    Wait(10)
                end
            end
            birdwings = CreateObject(0xC92962E3, GetEntityCoords(PlayerPedId())+vector3(0.0,0.0,10.0), true, true, true)
            SetEntityCollision(bird, false, false)
            bird = CreatePed(birdmodel, GetEntityCoords(PlayerPedId())+vector3(0.0,0.0,10.0), 0.0, true, true)
            Citizen.InvokeNative(0x283978A15512B2FE, bird, 1) --SetRandomOutfitVariation
            SetEntityAlpha(bird, 0)
            AttachEntityToEntity(birdwings, bird, GetEntityBoneIndexByName(bird, 'skel_head'), 0.0,0.0,0.0,  90.0,0.0,0.0, true, true, false, true, 1, true)
            return
        else
            birdmodel = GetHashKey(args[1]) or `A_C_Crow_01`
            if IsModelValid(birdmodel) then
                if not HasModelLoaded(birdmodel) then
                    RequestModel(birdmodel, true)
                    while not HasModelLoaded(npcModel) do
                        Wait(10)
                    end
                end
            else
                print(args[1]..' is not a valid model')
                return
            end
        end
    
        SetPlayerModel(PlayerId(), birdmodel, false)
        ped = PlayerPedId()
        Citizen.InvokeNative(0x283978A15512B2FE, ped, 1)
        PromptSetEnabled(FPrompt, true)
        PromptSetVisible(FPrompt, true)
        PromptSetEnabled(SpacebarPrompt, true)
        PromptSetVisible(SpacebarPrompt, true)
        isbird=true
        
        while isbird==true do
            Wait(0)
            DisableControlAction(0, 0xD9D0E1C0, true)
            if isairborne ~= true then --Is walking bird
                DisableFirstPersonCamThisFrame()
                PromptSetEnabled(FPrompt, true)
                if IsControlJustReleased(0, 0x639B9FC9) then --If Spacebar (0xD9D0E1C0)
                    pedheading = GetEntityHeading(ped)
                    if not HasModelLoaded(`mp_male`) then
                        RequestModel(`mp_male`, true)
                        while not HasModelLoaded(`mp_male`) do
                            Wait(10)
                        end
                    end
                    SetPlayerModel(PlayerId(), `mp_male`, false)
                    ped = PlayerPedId()
                    Citizen.InvokeNative(0x283978A15512B2FE, ped, 1) --SetRandomOutfitVariation
                    SetEntityCollision(ped, false, false)
                    SetEntityVisible(ped, false)
                    
                    bird = CreatePed(birdmodel, GetEntityCoords(ped), 0.0, true, true)
                    Citizen.InvokeNative(0x283978A15512B2FE, bird, 1) --SetRandomOutfitVariation
                    AttachEntityToEntity(ped, bird, GetEntityBoneIndexByName(bird, 'skel_head'), 0.0,0.0,0.0,  0.0,0.0,90.0, true, true, false, true, 1, true)
                    SetBlockingOfNonTemporaryEvents(bird, 1)
                    SetEntityHeading(bird, pedheading)
                    birdcoords = GetEntityCoords(bird)
                    isairborne=true
                    takeoff=true
                end
                if IsControlPressed(0, 0x3B24C470) then -- F
                    isbird=false
                    PromptSetEnabled(FPrompt, false)
                    PromptSetVisible(FPrompt, false)
                    PromptSetEnabled(SpacebarPrompt, false)
                    PromptSetVisible(SpacebarPrompt, false)
                    DisableControlAction(0, 0xD9D0E1C0, false)
                    ExecuteCommand('loadskin')          -- Replace the code on this like with what you want to happen when they "exit" being a bird
                end

            else --Is flying bird
                PromptSetEnabled(FPrompt, false)
                TaskFlyToCoord(bird, 1.0, GetOffsetFromEntityInWorldCoords(bird,0.0,10.0,-0.85), 1, 1) --Fly straight & level
                if IsControlPressed(0, 0x639B9FC9) then -- Spacebar (stops movement - 0xD9D0E1C0)
                    birdheading = GetEntityHeading(bird)
                    airborne = false
                    TaskFlyToCoord(bird, 1.0, GetOffsetFromEntityInWorldCoords(bird,0.0,1.0,0.0), 1, 1)
                    Wait(1500)
                    DetachEntity(ped, false, true)
                    DeleteEntity(bird)
                    SetPlayerModel(PlayerId(), birdmodel, true)
                    ped = PlayerPedId()
                    Citizen.InvokeNative(0x283978A15512B2FE, ped, 1) --SetRandomOutfitVariation
                    SetEntityCollision(ped, true, true)
                    SetEntityVisible(ped, true)
                    SetEntityHeading(ped, birdheading)
                    isairborne = false
                end

                ---NAVIGATION CONTROLS / AMOUNTS---

                if IsControlPressed(0, 0x7065027D) then -- A (left)
                    TaskFlyToCoord(bird, 1.0, GetOffsetFromEntityInWorldCoords(bird,-3.0,5.0,-0.5), 1, 1)
                end
                if IsControlPressed(0, 0xB4E465B4) then -- D (right)
                    TaskFlyToCoord(bird, 1.0, GetOffsetFromEntityInWorldCoords(bird,3.0,5.0,-0.5), 1, 1)
                end
                if IsControlPressed(0, 0x8FD015D8) then  -- W (up)
                    TaskFlyToCoord(bird, 1.0, GetOffsetFromEntityInWorldCoords(bird,0.0,5.0,5.0), 1, 1)
                end
                if IsControlPressed(0, 0xD27782E3) then -- S (down)
                    TaskFlyToCoord(bird, 1.0, GetOffsetFromEntityInWorldCoords(bird,0.0,5.0,-5.0), 1, 1)
                end
            end
        end
    end)
end)

-------------------
--   PROMPTS     --
-------------------
function FPrompt()
    FPrompt = PromptRegisterBegin()
    PromptSetControlAction(FPrompt, 0x3B24C470)
    str = CreateVarString(10, 'LITERAL_STRING', "Change Back")
    PromptSetText(FPrompt, str)
    PromptSetEnabled(FPrompt, false)
    PromptSetVisible(FPrompt, false)
    PromptRegisterEnd(FPrompt)
end

function SpacebarPrompt()
    SpacebarPrompt = PromptRegisterBegin()
    PromptSetControlAction(SpacebarPrompt, 0x639B9FC9)
    str = CreateVarString(10, 'LITERAL_STRING', "Takeoff/Land")
    PromptSetText(SpacebarPrompt, str)
    PromptSetEnabled(SpacebarPrompt, false)
    PromptSetVisible(SpacebarPrompt, false)
    PromptRegisterEnd(SpacebarPrompt)
end

--Initialize Prompts
FPrompt()
SpacebarPrompt()