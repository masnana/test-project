fx_version 'bodacious'
games {'rdr3'}
rdr3_warning 'I acknowledge that this is a prerelease build of RedM, and I am aware my resources *will* become incompatible once RedM ships.'
client_scripts {
	'fireworks_client.lua'
}

server_scripts {
	'fireworks_server.lua'
}